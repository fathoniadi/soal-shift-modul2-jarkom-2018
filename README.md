# Soal Shift Modul DNS dan Web Server

## Peraturan Soal Shift Modul 2 DNS dan Web Server
1. Waktu pengerjaan dari soal keluar, hingga hari Sabtu tanggal 24 Maret 2018 pukul 22.00 WIB.
2. **TIDAK DIPERBOLEHKAN BERTANYA KONFIGURASI KEPADA ASISTEN**. Jadikan google sebagai teman kalian.
3. Jika tidak ada pemberitahuan revisi soal dari asisten, berarti semua soal **BERSIFAT BENAR** dan **DAPAT DIKERJAKAN**
4. Plotting Asisten untuk demo dan revisi akan keluar pada hari Minggu 25 Maret 2018
5. Default memory 96M kecuali untuk Server diganti menjadi 128M.
6. Tidak Diperkenankan menambah memory tanpa persetujuan asisten, resiko tanggung sendiri akibatnya

## Penting untuk dibaca
1. File pendukung:

    a. File/folder pendukung untuk web http://caj.xxx.corp bisa download dengan cara :
            
        wget 10.151.36.5/caj.corp.tar.gz

    b. File/folder pendukung untuk web http://static.caj.xxx.corp bisa download dengan cara :

        wget 10.151.36.5/static.caj.corp.tar.gz

2. xxx adalah nama kelompok
3. Jika akan menginstall **php** maka gunakan **php5**. `apt-get install php5`
4. Mulai 21 Maret 2018 dari pihak DPTSI mematikan login proxy melalui email ITS. Oleh karena itu login proxy memalui Akun VPN bisa didapatkan di **https://id.its.ac.id/otp/**,
    
     `export http_proxy="http://usernameVPN:passVPN@proxy.its.ac.id:8080";`

     `export hhtps_proxy="http://usernameVPN:passVPN@proxy.its.ac.id:8080";`

     `export ftp_proxy="http://usernameVPN:passVPN@proxy.its.ac.id:8080";`

     contoh:

     ![](/images/vpn.png)


     `export http_proxy="http://ITS-545169-99513:d1a35@proxy.its.ac.id:8080";`

     `export hhtps_proxy="http://ITS-545169-99513:d1a35@proxy.its.ac.id:8080";`

     `export ftp_proxy="http://ITS-545169-99513:d1a35@proxy.its.ac.id:8080";`

Keterangan:
Gunakan VPN yang aktif yang memiliki status __ACTIVE__ pada kolom status. Kenapa kok dicontoh memakai yang __Non Active__?Cuma pingin kasih tau aja yang mana username dan passwordnya hehehe. 

### INI SOALNYA YA

![](/images/topologi.png)

Nanda adalah CEO dari perusahaan PT Cinta Abadi Jaya. Perusahaan tersebut bergerak pada bidang Per-WARKOP-an. Nanda ingin membuat website perusahaan untuk menunjang proses bisnis perusahaan tersebut. Nanda dibantu Ridho yang merupakan karyawannya awalnya menyiapkan infrastruktur jaringan dan komputer terlebih dahulu. **ALPHET** sebagai router, **WAYAHE** sebagai Client __CENGENGESAN__ sebagai DNS Server Master, __TJANGKIR__ sebagai DNS Server Slave dan __MENYENG__ sebagai Web Server.
(1) Awalnya mereka membuat website utama perusahaan dengan alamat __http://caj.xxx.corp__ (2) dan CNAME __http://www.caj.xxx.corp__, dan (3) __http://static.caj.xxx.corp__  yang diatur DNS-nya pada __CENGENGESAN__ dan diarahkan ke IP Server **MENYENG** serta dibuatkan (4) reverse domain. (5) Untuk antisipasi terjadi kerusakan server, mereka juga membuat DNS Server slave pada **TJANGKIR** agar service tidak mati. (6) Selain website utama mereka juga membuat web REST API dengan alamat __http://api.caj.xxx.corp__ yang didelegasikan pada server **TJANGKIR** dan diarahkan ke server **MENYENG**. Untuk menunjang proses bisnis perusahaan, mereka menggunakan Vmware Vsphere. Aplikasi tersebut dibuatkan api yang diarahkan ke server **MENYENG** dengan alamat (7) __http://vsphere.service.api.caj.xxx.corp__.

Setelah mengatur DNS, mereka segera mengatur web server. Untuk (8) web __http://caj.xxx.corp__, memiliki _Document root_ pada __/var/www/caj.xxx.corp__ dan (9) tidak bisa directory listing. Setelah mengatur virtual host, mereka segera mendeploy web utama mereka. Awalnya web dapat diakses menggunakan alamat (10) __http://caj.xxx.corp/index.php/home__. Karena dirasa alamat urlnya kurang cantik, maka Ridho mengaktifkan mod rewrite agar urlnya menjadi __http://caj.xxx.corp/home__. 

Web __http://static.caj.xxx.corp__ digunakan untuk tempat penyimpanan file assets yang memiliki Documentroot (11) __/var/www/static.caj.xxx.corp__ yang didalamnya memiliki folder sebagai berikut:
    
    /var/www/static.caj.xxx.corp
                            /public/javascripts
                            /public/css
                            /public/images
                            /bower_components
                            /errors


Pada folder folder (12) __/public__ diperbolehkan directory listing sedangkan lainnya tidak boleh. (13) Untuk mengatasi error HTTP Code 404. Ridho dan Nanda membuat file **404.html** yang berada pada folder __/errors__ untuk mengganti tampilan error 404 dari Apache. (14, 15, 16) Untuk mengakses file asset javascript, css, images awalnya harus menggunakan url

    http://static.caj.xxx.corp/public/javascripts

    http://static.caj.xxx.corp/public/css
    
    http://static.caj.xxx.corp/public/images 

Karena dirasa terlalu panjang maka Ridho mengatur konfigurasi virtual host agar ketika mengakses file asset menjadi 
    
    http://static.caj.xxx.corp/js

    http://static.caj.xxx.corp/css

    http://static.caj.xxx.corp/images

Untuk web **http://api.caj.xxx.corp** belum dapat diatur pada web server karena menunggu pengerjaan aplikasi api selesai. Namun pada web (17) **http://vsphere.service.api.caj.xxx.corp** sudah dapat diakses menggunakan port **8000** saja karena web masih dalam tahap pengembangan dan masih ada beberapa fitur yang error yang memiliki Documentroot pada __/var/www/vsphere__. Untuk mengakses web (18) **http://vsphere.service.api.caj.xxx.corp**, karyawan perusahaan harus menggunakan VPN (Virtual Private Network) yang memiliki ip **10.151.252.0/22**(Informatic Wifi, Netmask 255.255.252.0) dengan tujuan web tersebut tidak mudah diserang hacker.

Karena pada akhirnya Ridho sudah menyelesaikan semua tugasnya maka Nanda jatuh hati padanya. 

#### Sekian cerita dongeng Jarkom 2018 dan selamat mengerjakan 

# NB
## Setelah Plotting Asisten untuk demo keluar pada hari Minggu 25 Maret 2018, **DIWAJIBKAN** untuk menghubungi Asisten PENGUJI **MAKSIMAL** hari **SENIN 26 Maret 2018 jam 22.00 WIB**